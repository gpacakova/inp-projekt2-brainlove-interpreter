-- cpu.vhd: Simple 8-bit CPU (BrainLove interpreter)
-- Copyright (C) 2016 Brno University of Technology,
--                    Faculty of Information Technology
-- Author(s): xpacak01
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity cpu is
 port (
   CLK   : in std_logic;  -- hodinovy signal
   RESET : in std_logic;  -- asynchronni reset procesoru
   EN    : in std_logic;  -- povoleni cinnosti procesoru
 
   -- synchronni pamet ROM
   CODE_ADDR : out std_logic_vector(11 downto 0); -- adresa do pameti
   CODE_DATA : in std_logic_vector(7 downto 0);   -- CODE_DATA <- rom[CODE_ADDR] pokud CODE_EN='1'
   CODE_EN   : out std_logic;                     -- povoleni cinnosti
   
   -- synchronni pamet RAM
   DATA_ADDR  : out std_logic_vector(9 downto 0); -- adresa do pameti
   DATA_WDATA : out std_logic_vector(7 downto 0); -- mem[DATA_ADDR] <- DATA_WDATA pokud DATA_EN='1'
   DATA_RDATA : in std_logic_vector(7 downto 0);  -- DATA_RDATA <- ram[DATA_ADDR] pokud DATA_EN='1'
   DATA_RDWR  : out std_logic;                    -- cteni (1) / zapis (0)
   DATA_EN    : out std_logic;                    -- povoleni cinnosti
   
   -- vstupni port
   IN_DATA   : in std_logic_vector(7 downto 0);   -- IN_DATA <- stav klavesnice pokud IN_VLD='1' a IN_REQ='1'
   IN_VLD    : in std_logic;                      -- data platna
   IN_REQ    : out std_logic;                     -- pozadavek na vstup data
   
   -- vystupni port
   OUT_DATA : out  std_logic_vector(7 downto 0);  -- zapisovana data
   OUT_BUSY : in std_logic;                       -- LCD je zaneprazdnen (1), nelze zapisovat
   OUT_WE   : out std_logic                       -- LCD <- OUT_DATA pokud OUT_WE='1' a OUT_BUSY='0'
 );
end cpu;


-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of cpu is

 -- zde dopiste potrebne deklarace signalu
 
	type t_state is (
		shalt,
		ptr_increment,
		ptr_decrement,
		data_read_wait, data_read_in_req, data_read_in_write,
		data_read_inc,
		data_read_dec,
		data_read_print_0,
		data_read_print,
		data_read_in,
		while_begin, while_begin2, while_begin3, while_begin4, while_begin5,
		while_begin_wait,
		while_end, while_end2, while_end3,  while_end4, while_end5, while_end6,
		while_end_wait,
		sfetch0,
		sfetch1,
		sdecode,
		data_write_inc,
		data_write_dec,
		tmp_read,
		tmp_write
		);
		
	signal pstate, nstate: t_state;
	signal ptr : std_logic_vector (9 downto 0);
	signal pc : std_logic_vector(11 downto 0);
	signal cnt : std_logic_vector (7 downto 0);
	signal cnt_inc, cnt_dec, cnt_push : std_logic;
	signal pc_inc, pc_dec : std_logic := '0';
	signal ptr_inc, ptr_dec : std_logic := '0';
	signal tmp : std_logic_vector(7 downto 0);
	signal tmp_w: std_logic;
	signal mux_sel: std_logic_vector (1 downto 0);

begin

 -- zde dopiste vlastni VHDL kod
 OUT_DATA <= DATA_RDATA;
 
	nstate_logic: process (CLK,RESET,EN,nstate)
		begin
			if(RESET='1') then
				pstate <= sfetch0;
			elsif rising_edge(CLK) and EN = '1' then
				pstate <= nstate;
			end if;
		end process;
		
	program_counter: process(RESET,CLK,pc_inc,pc_dec,pc) 
		begin
			if(RESET='1') then
				pc <= (others => '0');
			elsif(rising_edge(CLK)) then
				if (pc_inc ='1') then
					pc <= pc + 1;
				elsif pc_dec = '1' then
					pc <= pc - 1; 
				end if;				
			end if;
			CODE_ADDR<=pc;
		end process;
		
	pointer_logic: process(CLK,RESET,ptr,ptr_inc,ptr_dec)
		begin
			if(RESET='1') then
				ptr <= (others => '0');
			elsif(rising_edge(CLK)) then
				if ptr_inc = '1' then
					ptr <= ptr + 1;
				elsif ptr_dec = '1' then
					ptr <= ptr - 1;
				end if;
			end if;
			DATA_ADDR<=ptr;
		end process;
		
	tmp_logic: process(CLK,RESET,tmp,tmp_w)
		begin
			if(RESET='1') then
				tmp <= (others =>'0');
			elsif(rising_edge(CLK)) then	
				if tmp_w='1' then
					tmp <= DATA_RDATA;
				end if;
			end if;
		end process;
				
	cnt_logic: process(RESET,CLK,cnt_inc,cnt_dec,cnt_push)
		begin
			if(RESET='1') then
				cnt <= (others=>'0');
			elsif(rising_edge(CLK)) then
				if(cnt_dec='1') then
					cnt <= cnt - 1;
				elsif(cnt_inc='1') then
					cnt <= cnt + 1;
				end if;	
			end if;
			if(cnt_push='1') then
				cnt <= "00000001";
			end if;
		end process;
		
	mux_logic: process(CLK,mux_sel,DATA_RDATA,IN_DATA,tmp)
		begin
			case mux_sel is
				when "00" =>
					DATA_WDATA <= DATA_RDATA + 1;
				when "01" =>
					DATA_WDATA <= DATA_RDATA - 1;
				when "10" =>
					DATA_WDATA <= tmp;
				when "11" =>
					DATA_WDATA <= IN_DATA;
				when others =>
				end case;
		end process;
		
	fsm_logic: process(pstate,OUT_BUSY,IN_VLD,CODE_DATA,DATA_RDATA,cnt,mux_sel)
		begin
			OUT_WE <= '0';
			CODE_EN <='0';
			DATA_EN <='0';
			IN_REQ <= '0';
			DATA_RDWR <= '0';
			pc_inc <= '0';
			pc_dec <='0';
			nstate <= sfetch0;
			tmp_w <= '0';
			ptr_inc <='0';
			ptr_dec <='0';
			cnt_inc <= '0';
			cnt_dec <= '0';
			cnt_push <= '0';
			mux_sel <= "ZZ";
			
		case pstate is
			
			when sfetch0 =>
				nstate <= sfetch1; -- prechadzam do stavu: nacitanie instr.
				
			when sfetch1 =>
				nstate <= sdecode; --rozpoznanie instr.
				CODE_EN <= '1';
				
			when sdecode =>
				
					case CODE_DATA is
						when X"3E" => nstate <= ptr_increment; -- >
						when X"3C" => nstate <= ptr_decrement; -- <
						when X"2B" => nstate <= data_read_inc; -- +
						when X"2D" => nstate <= data_read_dec; -- -
						when X"2E" => nstate <= data_read_print_0;-- .
						when X"2C" => IN_REQ <='1'; nstate <= data_read_in;-- ,
						when X"5B" => nstate <= while_begin_wait; -- [
						when X"5D" => nstate <= while_end_wait; -- ]
						when X"24" => DATA_RDWR <='1'; DATA_EN <='1'; nstate <= tmp_write; -- $
						when X"21" => DATA_RDWR <='1'; DATA_EN <='1'; nstate <= tmp_read; -- !
						when X"00" => nstate <= shalt;
						when others => pc_inc<='1'; nstate <= sfetch0;
					end case; 
			

-------- PTR -------------
				
			when ptr_increment => -- inkrementujem ukazatel
				nstate <= sfetch0;
				ptr_inc<='1';
				pc_inc<='1';				
				
			when ptr_decrement => -- dekrementujem ukazatel
				nstate <= sfetch0;
				ptr_dec<='1';
				pc_inc<='1';

------- TMP ----------------
				
			when tmp_write => -- zapisujem do pomocnej premennej
				nstate <= sfetch0;
				tmp_w <= '1';
				pc_inc<='1';
				
			when tmp_read => -- citam pomocnu premennu
				nstate <= sfetch0;
				pc_inc<='1';
				mux_sel <= "10";
				DATA_RDWR <= '0';
				DATA_EN <= '1';
				
---------- DATA + - ---------		
	
			when data_read_inc => -- cakam na data, ktore nasledne inkrementujem
				nstate <= data_write_inc;
				DATA_RDWR <='1';
				DATA_EN <='1';
				mux_sel <= "00";				
				
			when data_read_dec => -- cakam na data, kt nasledne dekrementujem
				nstate <= data_write_dec;
				DATA_RDWR <='1';
				DATA_EN <='1';
				mux_sel <= "01";				
				
			when data_write_inc => -- zapisem inkr. data
				nstate <= sfetch0;
				DATA_RDWR <='0';
				DATA_EN <='1';
				pc_inc<='1';
				mux_sel <= "00";
				
			when data_write_dec => -- zapisem dekr. data
				nstate <= sfetch0;
				DATA_RDWR <='0';
				DATA_EN <='1';
				pc_inc<='1';
				mux_sel <= "01";

------ PRINT . --------			
			when data_read_print_0 => --cakaci stav na tisknutie dat
				nstate <= data_read_print;
				DATA_RDWR <='1';
				DATA_EN <='1';
				
			when data_read_print => -- vytisknem data, pockam kym LCD nieje zaneprazdneny predoslym zapisom
				nstate <= data_read_print;
				--DATA_RDWR <='1';
				--DATA_EN <='1';

				if(OUT_BUSY = '0') then
					nstate <= sfetch0;
					pc_inc<='1';
					OUT_WE <='1';
					--OUT_DATA <= DATA_RDATA;
				end if;
				
------ IN_DATA , ----------	
			
			when data_read_in=>
				nstate <= data_read_in; -- cakam tu, az kym vstup nieje platny
				IN_REQ <='1';
				
				if(IN_VLD = '1') then -- cakam na data ktore ziskam z klavesnice a pripravujem zapis
					nstate <= data_read_wait;
					mux_sel <= "11";
					DATA_EN <='1';
					DATA_RDWR <='0';
				end if;
				
				
			when data_read_wait => -- zapisem
				nstate <= sfetch0;
				mux_sel <= "11";
				DATA_EN <='1';
				DATA_RDWR <='0';
				pc_inc<='1';				
				
--------- WHILE --------------		
		
			when while_begin_wait => nstate <= while_begin; -- cakaci stav pre zaciatok cyklu
			when while_end_wait => nstate <= while_end; -- cakaci stav pre koniec cyklu
				
-------- WHILE BEGIN ----------
	
			when while_begin=> -- zacinam cyklus [
				DATA_EN<='1';
				DATA_RDWR<='1';
				pc_inc<='1'; -- PC = PC + 1
				nstate<=while_begin2;
		
			when while_begin2=>
				if(DATA_RDATA="00000000") then --if(ram[PTR]==0)
					cnt_push<='1'; -- CNT <= 1
					nstate<=while_begin3; --do
				else
					nstate<=sfetch0;
				end if;
	
			when while_begin3=>
				if(cnt="00000000") then
					nstate<=sfetch0;
				else --while(CNT!=0)
					CODE_EN<='1'; -- c <= rom[PC]
					nstate<=while_begin4;
				end if;
	
			when while_begin4=>
				nstate<=while_begin5;
				if(CODE_DATA=X"5D") then --if c==]
					cnt_dec<='1'; --cnt--
				elsif(CODE_DATA=X"5B") then --elsif c==[
					cnt_inc<='1'; --cnt++
				end if;
	
			when while_begin5=>
				pc_inc<='1'; -- PC = PC + 1
				nstate<=while_begin3;--loop

-------- WHILE END -----------

			when while_end=> --koncim cyklus ]
				DATA_EN<='1';
				DATA_RDWR<='1';
				nstate<=while_end2;
	
			when while_end2=>
				if(DATA_RDATA="00000000") then -- if ram[PTR]==0
					nstate<= sfetch0;
					pc_inc<='1'; -- PC=PC+1
				else --if ram[PTR]!=0 
					cnt_push<='1';--CNT <= 1
					pc_dec<='1'; --PC=PC-1
					nstate<=while_end3; 
				end if;

			when while_end3=>
				if(cnt="00000000") then
					nstate<=sfetch0;
				else
					nstate<=while_end4;--while(CNT!=0)
				end if; 
	
			when while_end4=>
				CODE_EN<='1'; --c<=rom[PC]
				nstate<=while_end5;
		
			when while_end5=>
				nstate<=while_end6;
				if(CODE_DATA=X"5B") then--if c==[
					cnt_dec<='1';--CNT--
				elsif(CODE_DATA=X"5D") then--if c==]
					cnt_inc<='1';--CNT++
				end if;
	
			when while_end6=>
				nstate<=while_end3;
				if(cnt="00000000") then
					pc_inc<='1'; --PC=PC+1
				else
					pc_dec<='1'; --PC=PC-1
				end if;

------- OTHER --------
	
			when shalt => nstate<=shalt;
			when others => pc_inc<='1'; nstate <= sfetch0;
		end case;
	end process;
end behavioral;