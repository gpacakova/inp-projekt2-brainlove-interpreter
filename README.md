# README #

INP
Procesor s Harvardskou architekturou
Datum zadani: 21.10.2016
Autor: xpacak01
Hodnotenie: 20/23 b (-3b za upload nespravneho login.b suboru)

Cillem tohoto projektu je implementovat pomoci VHDL procesor s Harvardskou architekturou, ktery bude
schopen vykonavat program napsany v jazyce Brainlove, ktery je rozsirenim jazyka Brainfuck.

Pozn.: Nasledujuce zdrojove kody sluzia ako inspiracia a pomoc, pre pripad, ze sa sami neviete pohnut dalej. Prosim berte do uvahy,
		ze plagiatorstvo sa hodnoti 0 bodmi, neudelenim zapoctu a dalsim adekvatnym postihom podla platneho disciplinarneho radu VUT.
		V preklade: Skuste v prvom rade pouzit vlastnu hlavu a rozsirit svoj obzor o nove vedomosti. Koniec koncov to je jeden z dovodov preco chodime na FIT.
		
PS: Nasledujuce riesenie nemusi byt bezchybne alebo/ani optimalne. #notaVHDLperson